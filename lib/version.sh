#!/usr/bin/env bash
#
#
#
#

__repo__='https://gitlab.com/bschaves/storecli'
__online_pkg__='https://gitlab.com/bschaves/storecli/-/archive/main/storecli-main.zip'
__version__='2024-04-28'
__appname__='storecli'
__author__='Bruno Chaves'
