#!/usr/bin/env bash
#
# 
# Esse arquivo inicializa variáveis, e o core de funções shell-libs
# 
#
#


# SHELL_LIBS -> deve apontar para esse diretório 

if [[ -z $SHELL_LIBS ]]; then
	echo -e "Erro -> o diretório SHELL_LIBS não foi definido"
	exit 1
fi

SHELL_DEVICE_FILE="/tmp/$(whoami)-device.txt"
SHELL_STATUS_FILE="/tmp/$(whoami)-status.txt"
export STATUS_OUTPUT=0

#mkdir -p /tmp/$(whoami)
touch "$SHELL_DEVICE_FILE"
touch "$SHELL_STATUS_FILE"

source ~/.bashrc

[[ -z $HOME ]] && HOME=~/

# Inserir ~/.local/bin em PATH se não existir.
echo "$PATH" | grep -q "$HOME/.local/bin" || {
	export PATH="$HOME/.local/bin:$PATH"
}

function show_not_found_module(){
	echo -e "[ERRO] módulo não encontrado -> $@"
}

export readonly LIB_COLORS="${SHELL_LIBS}/common/colors.sh"
export readonly LIB_PRINT_TEXT="${SHELL_LIBS}/common/print_text.sh"
export readonly LIB_STRING="${SHELL_LIBS}/common/string.sh"
export readonly LIB_UTILS="${SHELL_LIBS}/common/utils.sh"
export readonly LIB_SYSTEM="${SHELL_LIBS}/system/sys.sh"
export readonly LIB_PLATFORM="${SHELL_LIBS}/system/platform.sh"
export readonly LIB_APP_DIRS="${SHELL_LIBS}/system/appdirs.sh"
export readonly LIB_REQUESTS="${SHELL_LIBS}/request/requests.sh"
export readonly LIB_APT_BASH="${SHELL_LIBS}/system/apt-bash.sh"
export readonly LIB_CRIPTO="${SHELL_LIBS}/system/crypto.sh"

# Importar módulos common
source "${LIB_COLORS}" 1> /dev/null 2>&1 || show_not_found_module "${LIB_COLORS}"
source "${LIB_PRINT_TEXT}" 1> /dev/null 2>&1 || show_not_found_module "${LIB_PRINT_TEXT}"
source "${LIB_STRING}" 1> /dev/null 2>&1 || show_not_found_module "${LIB_STRING}"
source "${LIB_UTILS}" 1> /dev/null 2>&1 || show_not_found_module "${LIB_UTILS}"

# Importar módulos system
source "${LIB_SYSTEM}" 1> /dev/null 2>&1 || show_not_found_module "${LIB_SYSTEM}"
source "${LIB_PLATFORM}" 1> /dev/null 2>&1 || show_not_found_module "${LIB_PLATFORM}"
source "${LIB_APP_DIRS}" 1> /dev/null 2>&1 || show_not_found_module "${LIB_APP_DIRS}"
source "${LIB_APT_BASH}" 1> /dev/null 2>&1 || show_not_found_module "${LIB_APT_BASH}"
source "${LIB_CRIPTO}" 1> /dev/null 2>&1 || show_not_found_module "${LIB_CRIPTO}"

# Importar módulos request
source "${LIB_REQUESTS}" 1> /dev/null 2>&1 || show_not_found_module "${LIB_REQUESTS}"







