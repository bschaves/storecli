#!/usr/bin/env bash
#

file_setup=$(readlink -f $0)
dir_of_project=$(dirname $file_setup)
force_install=false

source "${dir_of_project}"/common/version.sh


if [[ $(id -u) == 0 ]]; then
    PREFIX='/usr/local/lib'
    CONFIG_FILE_SHELL_LIBS='/etc/shell-libs.conf'
    SCRIPT_USER_PATH='/usr/local/bin/user-path'
else
    PREFIX=~/.local/lib
    CONFIG_FILE_SHELL_LIBS=~/shell-libs.conf
    SCRIPT_USER_PATH=~/.local/bin/user-path
    mkdir -p "$PREFIX"
    mkdir -p ~/.local/bin
    mkdir -p ~/bin
fi

SHELL_LIBS="${PREFIX}/shell-libs"



function create_local_dirs(){

    mkdir -p "$SHELL_LIBS"

}



function copy_files_shell_libs(){

    if [[ -d "$SHELL_LIBS" ]]; then
        if [[ "$force_install" == false ]]; then
            echo -e "SHELL_LIBS já instalado em ${SHELL_LIBS} "
            echo -e "use: -u|--uninstall para desistalar a versão anterior"
            return 0
        fi
    fi


    echo -e "Instalando -> $__appname__ V${__version__} => $SHELL_LIBS"
    [[ -d "$SHELL_LIBS" ]] && rm -rf "${SHELL_LIBS}"
    
    create_local_dirs
    cp -R "${dir_of_project}"/__init__.sh "${SHELL_LIBS}"/
    cp -R "${dir_of_project}"/setup.sh "${SHELL_LIBS}"/ 
    cp -R "${dir_of_project}"/request "${SHELL_LIBS}"/ 
    cp -R "${dir_of_project}"/common "${SHELL_LIBS}"/   
    cp -R "${dir_of_project}"/system "${SHELL_LIBS}"/
    cp -R "${dir_of_project}"/scripts "${SHELL_LIBS}"/    
    echo "OK"
}



function add_script_user_path()
{
    #

    [[ $(id -u) == 0 ]] && return 1
    mkdir -p ~/.local/bin 
    chmod +x "${SHELL_LIBS}"/scripts/user-path.sh
    #ln -sf "${SHELL_LIBS}"/scripts/user-path.sh "${SCRIPT_USER_PATH}"
    "${SHELL_LIBS}"/scripts/user-path.sh
}



function add_shell_libs_conf()
{

    [[ $(id -u) == 0 ]] && return 1

    echo -e "export SHELL_LIBS=${SHELL_LIBS}" > ~/shell-libs.conf
    grep -q "^source $CONFIG_FILE_SHELL_LIBS" ~/.bashrc && return
    echo -e "source $CONFIG_FILE_SHELL_LIBS" >> ~/.bashrc
}



function _uninstall_shell_libs(){

    if [[ ! -d "$SHELL_LIBS" ]]; then
        echo -e "[!] Diretório não encontrado -> ${SHELL_LIBS}"
        return 1
    fi

    echo -e "[+] Desinstalando shell-libs -> ${SHELL_LIBS}"
    [[ -d "$SHELL_LIBS" ]] && rm -rf "${SHELL_LIBS}"
    echo -e "" > "$CONFIG_FILE_SHELL_LIBS"

}


function usage(){

    echo -e " $__appname__ V$__version__\n\n"
    echo -e " Use:"
    echo -e " -h|--help        Exibe ajuda"
    echo -e " -u|--uninstall   Desinstalar o shell-libs"
    echo -e " -f|--force       Sobreescre uma versão anterior pela versão desse pacote"


}

function main(){

    if [[ "$1" == '-f' ]] || [[ "$1" == '--force' ]]; then force_install=true; fi

    case "$1" in
        -h|--help) usage; return;;
        -u|--uninstall) _uninstall_shell_libs; return;;
        *) 
            copy_files_shell_libs
            add_shell_libs_conf
            add_script_user_path
            ;;
    esac
}



main $@





