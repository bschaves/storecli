#!/usr/bin/env bash
#
#=============================================================================#
# REFERÊNCIAS
#=============================================================================#
# https://terminalroot.com.br/2015/07/30-exemplos-do-comando-sed-com-regex.html
#
#
#=============================================================================#
# Script para criar os seguintes diretórios na HOME do usuário:
#  ~/bin, ~/.local/bin, ~/opt e ~/.local/lib
#
#
#
#

# Proteção contra o root.
if [[ $(id --user) == 0 ]]; then
	echo '[!] Erro -> você não pode ser o root.'
	exit 1
fi



__this_appname__='user-paths'
__this_version__='2024-04-18'

[[ -z $HOME ]] &&  HOME=~/

export _local_prefix="$HOME"

export LOCAL_BIN="${_local_prefix}/.local/bin"
export HOME_BIN="${_local_prefix}/bin"
export HOME_OPT="${_local_prefix}/opt"
export LOCAL_LIB="${_local_prefix}/.local/lib"
export BACKUP_DIR="${HOME}/user-path-backups"

export config_file_user_paths=~/add-local-paths.sh
touch "$config_file_user_paths"

# Inserir ~/.local/bin em PATH.
#echo -e "$PATH" | grep -q "${LOCAL_BIN}" || PATH="$PATH:$LOCAL_BIN"
#echo -e "$PATH" | grep -q "${HOME_BIN}" || PATH="$PATH:$HOME_BIN"


function crate_dirs(){
	
	mkdir -p "$HOME_BIN"
	mkdir -p "$HOME_OPT"
	mkdir -p "$LOCAL_BIN"
	mkdir -p "$LOCAL_LIB"
	mkdir -p "$BACKUP_DIR"

}



function __get_time(){
	# Exibe a data formatada no stdout
	date | cut -d ' ' -f 1-4 | sed 's/ /-/g'
}



function _execute_backup_shell_files(){

	local output_backup_bashrc="${BACKUP_DIR}/bashrc-$(__get_time)"
	local output_backup_zshrc="${BACKUP_DIR}/zshrc-$(__get_time)"

	if [[ -f "$output_backup_zshrc" ]] || [[ -f "$output_backup_bashrc" ]]; then
		# Backup já executado anteriormente.
		return 0
	fi


	# ~/.bashrc
	if [[ -f ~/.bashrc ]]; then
		echo "Criando backup -> ${output_backup_bashrc}"
		cp -v ~/.bashrc "$output_backup_bashrc"
	fi


	# ~/.zshrc
	if [[ -f ~/.zshrc ]]; then
		echo "Criando backup -> ${output_backup_zshrc}"
		cp -v ~/.bashrc "$output_backup_bashrc"
	fi

}


function _configure_file_this_script(){
	local _line_config_local_bin="echo -e \$PATH | grep -q ${LOCAL_BIN} || export PATH=\$PATH:${LOCAL_BIN}"
	local _line_config_home_bin="echo -e \$PATH | grep -q ${HOME_BIN} || export PATH=\$PATH:${HOME_BIN}"


	if ! grep -q "^#!/usr/bin/env bash" "$config_file_user_paths"; then
		#sed -i '1s/^/\#!\/usr\/bin\/env bash/' "$config_file_user_paths"
		echo '#!/usr/bin/env bash' > "$config_file_user_paths"
		echo -e "\n" >> "$config_file_user_paths"
	fi
	
	# Verificar se LOCAL_BIN está em bashrc
	grep -q "$_line_config_local_bin" "$config_file_user_paths" || {
		# Adicionar LOCAL_BIN em bashrc
		echo -e "$_line_config_local_bin" >> "$config_file_user_paths"
		}



	# Verificar se HOME_BIN está em bashrc	
	grep -q "$_line_config_home_bin" "$config_file_user_paths" || {
		# Adicionar HOME_BIN em bashrc
		echo -e "$_line_config_home_bin" >> "$config_file_user_paths"
		}

	#
	#

}



function _configure_bashrc(){

	grep -q "^source ${config_file_user_paths}" ~/.bashrc && return 0
	echo -e "source ${config_file_user_paths}" >> ~/.bashrc

}

function _configure_zsh(){

	grep -q "^source ${config_file_user_paths}" ~/.zshrc && return 0
	echo -e "source ${config_file_user_paths}" >> ~/.zshrc

}




function configure_shell(){

	_execute_backup_shell_files

	echo "Configurando $config_file_user_paths"
	_configure_file_this_script
	[[ -x $(command -v bash) ]] && _configure_bashrc
	[[ -x $(command -v zsh) ]] && _configure_zsh

}



function usage(){
	echo -e "   $__this_appname__ V$__this_version__\n"
	echo -e " Use: "
	echo -e " -h|--help              Exibe ajuda"
	#echo -e " -H|--home-dir <dir>    informar qual pasta dever ser usada como HOME do usuário, o padrão é ~/"
}



function main(){

	crate_dirs

	case "$1" in
		-h|--help) usage; return 0;;
		*) configure_shell;;
	esac


}


main "$@"