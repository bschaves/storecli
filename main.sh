#!/usr/bin/env bash
#
#
#


#clear

__script__=$(readlink -f $0)
dir_of_project=$(dirname $__script__)

DIR_APPS_INFO="$dir_of_project"/installer-apps
STORECLI_LIB_PATH="${dir_of_project}"/lib
STORECLI_LIB_INFO_PATH="${dir_of_project}"/installer-apps

readonly STORECLI_CONFIG_FILE=~/.storeclirc

AssumeYes=False
DownloadOnly=False

# Importar shell-libs
export SHELL_LIBS="${dir_of_project}"/shell-libs
source "${SHELL_LIBS}"/__init__.sh

source "$STORECLI_LIB_PATH"/version.sh
source "$STORECLI_LIB_PATH"/manager.sh
source "$STORECLI_LIB_PATH"/show.sh
source "$STORECLI_LIB_PATH"/common.sh
source "$STORECLI_LIB_PATH"/requeriments.sh


# Configuração inicial
check_sum=true # Váriavel para informar se as somas hash devem ou não serem executadas

function set_initial_config(){

    if [[ ! -f "$STORECLI_CONFIG_FILE" ]]; then return; fi

    # Retornar a configuração do arquivo true/false
    check_sum=$(grep -m 1 'check_sum' "$STORECLI_CONFIG_FILE" | cut -d ' ' -f 2)

}


function _startApp(){
    # Verificar configurações básicas antes de iniciar a execução desse script
    #

    local script_config_user_path="${SHELL_LIBS}/scripts/user-path.sh"

    if [[ ! -x "$script_config_user_path" ]]; then
        [[ -w "$script_config_user_path" ]] && chmod +x "$script_config_user_path"
    fi

    # Configurar o PATH do usuário.
    "$script_config_user_path"

    # Checar dependências
    if ! "${dir_of_project}"/scripts/check.sh; then    
        InstallSysRequeriments
        return 1
    fi
}


function createStorecliDirs()
{
    mkdir -p $(getCacheDir)    
    mkdir -p $(getCachePkgs)
    mkdir -p $(getConfigDir)

    if [[ $(id -u) == 0 ]]; then return 0; fi

    mkdir -p $DIR_BIN
    mkdir -p $DIR_LIB
    mkdir -p $DIR_SHARE
    mkdir -p $DIR_THEMES
    mkdir -p $DIR_OPTIONAL
    mkdir -p $DIR_DESKTOP_ENTRY
    mkdir -p $DIR_ICONS
    mkdir -p $DIR_HICOLOR
}

function usage(){
cat << EOF
    Use: storecli --install|--uninstall|--download-only|--yes

    -i|--install        Instala um ou mais pacotes
    -u|--uninstall      Desistala um ou mais pacotes.
    
    -l|--list           Exibe os pacotes que podem ser instalados.
    -v|--version        Mostra versão
    
    -U|--self-update    Atualiza esta programa para ultima versão.
    -h|--help           Mostra ajuda.

    --clean-cache       Remove os arquivos baixados em cache.

    --info <app>        Exibe informações sobre um pacote.
  
EOF

}


function _parseOpts(){

    for option in "$@"; do
        if [[ $option == '-y' ]] || [[ $option == '--yes' ]]; then
            export AssumeYes='True'
        elif [[ $option == '-d' ]] || [[ $option == '--download-only' ]]; then
            export DownloadOnly='True'
        fi
    done

}

function self_update(){
    local script_update="${dir_of_project}"/scripts/update.sh

    chmod +x "${script_update}"
    "$script_update"
}

function clean_cache(){

    cd $(getCachePkgs) && {

        echo -e "Limpando cacahe em ... $(pwd)"
        rm -rf *
    }

}


function main()
{

    _startApp || return 1
    set_initial_config
    createStorecliDirs 
    _parseOpts $@

   
    if [[ "$1" == '-h' ]] || [[ $1 == '--help' ]]; then
        usage
        return
    elif [[ "$1" == '-v' ]] || [[ $1 == '--version' ]]; then
        echo -e "$__appname__ V${__version__}"
        return
    elif [[ "$1" == '-l' ]] || [[ $1 == '--list' ]]; then
        showApps
        return 0
    elif [[ "$1" == '-U' ]] || [[ $1 == '--self-update' ]]; then
        self_update
        return 0
    elif [[ "$1" == '-i' ]] || [[ $1 == '--install' ]]; then
        shift
        InstallPackages "$@"
    elif [[ "$1" == '-u' ]] || [[ $1 == '--uninstall' ]]; then
        shift
        UninstallPackages "$@"
    elif [[ $1 == '--info' ]]; then
        shift
        appInfo $@
        return 0
    elif [[ "$1" == '--clean-cache' ]]; then
        clean_cache
    fi


}


main $@


