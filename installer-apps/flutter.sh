#!/usr/bin/env bash
#
# https://docs.flutter.dev/get-started/install/linux
#
# https://storage.googleapis.com/flutter_infra_release/releases/stable/linux/flutter_linux_3.19.3-stable.tar.xz 
#

setDirsUser

INSTALLATION_TYPE='user'
APP_NAME='flutter'
DESTINATION_DIR="${DIR_OPTIONAL}"/flutter-linux

ICON_FILE=''
DESKTOP_FILE=''
PKG_FILE="$(getCachePkgs)"/flutter_linux_3.19.3-stable.tar.xz
SCRIPT_FILE="${DIR_BIN}"/flutter # DIR_BIN é fornecido por shell-libs
BIN_FILE="${DESTINATION_DIR}/bin/flutter"

LINK_FILE=None
APP_VERSION='3.19'

PKG_URL='https://storage.googleapis.com/flutter_infra_release/releases/stable/linux/flutter_linux_3.19.3-stable.tar.xz'
ONLINE_SIZE=''

HASH_TYPE='sha256'
HASH_VALUE=''


function _uninstall_flutter()
{
	echo -e "Desinstalando ... $APP_NAME"
    rm -rf "$DESTINATION_DIR" 
    rm -rf $ICON_FILE
    rm -rf $DESKTOP_FILE
    return 0
}


function _install_flutter()
{
	# Instalar o android_studio.
	isRoot && {
		printErro "Você não pode ser o root."
		return 1
	}


	if [[ -d "$DESTINATION_DIR" ]]; then
        printErro "Desinstale a versão atual de ... $APP_NAME para prosseguir"
        sleep 0.2
        return 1
    fi

    # Verificar integridade.
    #checkSha256 $PKG_FILE $HASH_VALUE || return $?

    local _tmp_dir=$(mktemp -d)
    local _tmp_file=$(mktemp)
    
    unpackArchive $PKG_FILE $_tmp_dir || return $?
    #unpackArchive $PKG_FILE /home/bruno/TMP
    green "Entrando no diretório ... $_tmp_dir"
    cd $_tmp_dir || return 1

    green "Copiando arquivos"
    mv flutter "$DESTINATION_DIR"
    cd "$DESTINATION_DIR"/bin
    chmod +x flutter
    ln -sf "${DESTINATION_DIR}/bin/flutter" "$SCRIPT_FILE"

    
    rm -rf $_tmp_dir
    rm -rf $_tmp_file
    return 0
}


function main()
{	
	
	
	if [[ $1 == 'uninstall' ]]; then
        _uninstall_flutter 
    elif [[ $1 == 'install' ]]; then
        _install_flutter
    elif [[ $1 == 'get' ]]; then
        download $PKG_URL $PKG_FILE || return $?
    elif [[ $1 == 'installed' ]]; then
        isExecutable android_studio || return 1
    else
        printErro 'Parâmetro incorreto.'
        return 1
    fi

    return $?	
}


# main $@