# storecli
# Sua loja de aplicativos via linha de comando.

INFO:

storecli --help           => Ajuda

storecli --list           => Lista pacotes disponíveis para instalação.

storecli install <pacote> => Instala um pacote

storecli remove <pacote> => Remove um pacote

storecli                 => Abre o GUI gráfico
