#!/usr/bin/env bash
#
# https://www.blogopcaolinux.com.br/2022/11/Guia-pos-instalacao-Fedora-37-Workstation.html
#
# https://docs.fedoraproject.org/en-US/quick-docs/rpmfusion-setup/
#
#


if [[ $(id -u) == 0 ]]; then
	echo -e "\n ! ERRO ... você não poder o root."
	echo
	exit 1
fi

function print_line(){

	echo -e "*********************************************"
}

function msg(){

	print_line
	echo -e " * $@"
	print_line
}

function install_gnome_config(){

	msg "Instalando ferramentas do gnome"

	sudo dnf install gnome-tweaks
	sudo dnf install gnome-browser-connector
	sudo dnf install gnome-extensions-app

}


function add_repos(){

	msg "Adicionando repositórios."

	sudo dnf install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
	sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
	sudo dnf install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

	sudo dnf config-manager --enable fedora-cisco-openh264

	flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
}

function install_codecs(){

	msg "Instalando codecs"

	sudo dnf install gstreamer1-plugins-{bad-\*,good-\*,base} gstreamer1-plugin-openh264 gstreamer1-libav --exclude=gstreamer1-plugins-bad-free-devel
	sudo dnf install amrnb amrwb faad2 flac ffmpeg gpac-libs libde265 libfc14audiodecoder mencoder x264 x265

}

function install_players(){

	msg "Instalando player de vídeo."

	sudo dnf install vlc
	sudo dnf install smplayer
	sudo dnf install HandBrake HandBrake-gui
}

function install_apps(){

	msg "Instalando aplicativos"

	sudo dnf install inkscape
	sudo dnf install telegram-desktop

}


function install_browsers(){

	msg "Instalando navegadores"

	sudo dnf install 'https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm'

}

# Habilitar ou desabilitar o FireWalld
# sudo systemctl disable firewalld.service
# sudo systemctl enable firewalld.service
# sudo systemctl status firewalld.service


#sudo dnf install system-config-language
#system-config-language



function main(){

	install_gnome_config
	add_repos
	install_codecs
	install_players
	install_apps
	install_browsers

}

main $@